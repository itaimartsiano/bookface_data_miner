import csv
import sys

from data_miner import DataMiner
from users_map_builder import build_users_list
from utils.logger_configuration import configure_logger


def save_to_csv(users_list):
    # the file will include the data with ',' separator in this order:
    # user_id, name, age, favorite_color, followers, following
    with open('data.csv', 'w') as csv_file:
        wr = csv.writer(csv_file, delimiter=',')
        for user_data in users_list:
            wr.writerow(user_data.get_row())
    logger.debug("Data saved to data.csv file")


if __name__ == "__main__":
    logger = configure_logger()
    logger.debug("DataMiner started")

    if len(sys.argv) is 3:
        username = sys.argv[1]
        password = sys.argv[2]
    else:
        username = "user1"
        password = "pass1"
    logger.info("DataMiner will try connect site with username: " + username)

    data_extractor = DataMiner(username=username, password=password)
    users_list = build_users_list(data_extractor=data_extractor)
    data_extractor.close()

    save_to_csv(users_list)
