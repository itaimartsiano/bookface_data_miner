import sys

from data_structure.unique_queue import UniqueQueue
from data_structure.user_data import UserData
from utils.constants import DEFAULT_MAX_USERS_MINING
from utils.logger_configuration import get_logger

logger = get_logger()


def build_users_list(data_extractor):
    users_map_by_id = build_partial_users_map(data_extractor)
    add_following_data(users_map_by_id)
    return users_map_by_id.values()


def build_partial_users_map(data_extractor):
    max_user_to_mine = get_max_users_to_mine()
    followers_id_to_data_map = {}

    non_visited_users = UniqueQueue()
    visited_users = set()

    non_visited_users.enqueue('me')

    while not non_visited_users.is_empty() and len(visited_users) < max_user_to_mine:
        user_id = non_visited_users.deque()
        followers = get_user_followers(data_extractor, user_id)

        for follower_id, follower_data in followers.items():
            if (follower_id in visited_users) or (non_visited_users.contains(follower_id)):
                continue
            non_visited_users.enqueue(follower_id)

        add_user_to_result(data_extractor, followers, followers_id_to_data_map, user_id)
        visited_users.add(user_id)
        logger.debug('added user_id ' + str(user_id) + ', total users inserted is: ' + str(len(visited_users)))
    return followers_id_to_data_map


def get_user_followers(data_extractor, user_id):
    if user_id is 'me':
        followers = data_extractor.get_root_user_followers_selenium()
    else:
        followers = data_extractor.get_user_followers_selenium(user_id=user_id)
    # this call is not reliable, the server returns 500 internal error frequently, leave it here in case it will work
    # followers = data_extractor.get_user_followers(user_id=user_id)
    return followers


def add_user_to_result(data_extractor, followers, followers_id_to_data_map, user_id):
    name, age, favorite_color = data_extractor.get_user_data()
    user_data = UserData(user_id=user_id, name=name, age=age, favorite_color=favorite_color,
                         followers=list(followers.keys()), following=list())
    followers_id_to_data_map[user_id] = user_data


def add_following_data(users_map):
    for user_id, user_data in users_map.items():
        for follower_id in user_data.followers:
            if follower_id in users_map:
                follower_data = users_map[follower_id]
                follower_data.following.append(user_id)


def get_max_users_to_mine():
    if len(sys.argv) is 4:
        return int(sys.argv[3])
    else:
        return DEFAULT_MAX_USERS_MINING
