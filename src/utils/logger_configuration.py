import logging
import sys

LOGGER_NAME = 'bookface_data_miner'


def configure_logger():
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    logger = logging.getLogger(LOGGER_NAME)
    logger.setLevel(logging.DEBUG)

    file_handler = logging.FileHandler('bookface_miner.log', mode='w')
    file_handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    logger.addHandler(screen_handler)
    return logger


def get_logger():
    return logging.getLogger(LOGGER_NAME)
