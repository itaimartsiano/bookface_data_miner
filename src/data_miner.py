import json
import re

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from utils.constants import URL_LOGIN, CHROME_DRIVER_PATH, URL_ME, FAVORITE_COLOR_CLASS_NAME, FOLLOWERS_CLASS_NAME, \
    URL_USER, URL_LOGIN_SCREEN
from utils.logger_configuration import get_logger

logger = get_logger()


class DataMiner:

    def __init__(self, username, password):
        self.__login(username, password)
        self.driver = webdriver.Chrome(CHROME_DRIVER_PATH)
        self.driver.get(URL_LOGIN_SCREEN)
        WebDriverWait(self.driver, 15).until(ec.presence_of_element_located((By.TAG_NAME, "input")))
        self.__add_session_cookie_to_chrome()

    def __login(self, username, password):
        self.s = requests.session()
        self.s.post(url=URL_LOGIN, json={"username": username, "password": password})
        logger.info("DataMiner connected to website")

    def close(self):
        self.s.close()
        self.driver.close()
        logger.info("DataMiner closed all connections")

    def get_root_user_followers_selenium(self):
        self.driver.get(URL_ME)
        return self.__get_followers_with_selenium()

    def get_user_followers_selenium(self, user_id):
        self.driver.get(URL_USER + user_id)
        return self.__get_followers_with_selenium()

    def __get_followers_with_selenium(self):
        WebDriverWait(self.driver, 15).until(ec.presence_of_element_located((By.CLASS_NAME, FOLLOWERS_CLASS_NAME)))
        self.driver.find_elements_by_xpath("//a[@href]")[0].get_attribute("href")
        followers_id_to_follower = {}
        href_list = self.driver.find_elements_by_xpath("//a[@href]")
        for href in href_list:
            url_link = href.get_attribute("href")
            user_id = url_link.split("/")[-1]
            followers_id_to_follower[user_id] = user_id
        return followers_id_to_follower

    def get_user_data(self):
        # We assume the driver is already set on the user_id page
        WebDriverWait(self.driver, 15).until(ec.presence_of_element_located((By.CLASS_NAME, FAVORITE_COLOR_CLASS_NAME)))

        name = self.driver.find_elements_by_xpath("//p[contains(text(), 'Name:')]")[0].text.split("Name: ")[1]
        age = self.driver.find_elements_by_xpath("//p[contains(text(), 'Age:')]")[0].text.split("Age: ")[1]
        favorite_color = self.__get_favorite_color_in_rgb()

        return name, age, favorite_color

    def __get_favorite_color_in_rgb(self):
        element = self.driver.find_element_by_class_name(FAVORITE_COLOR_CLASS_NAME)
        rgba = element.value_of_css_property('background-color')
        r, g, b, a = re.search(r'rgba\((\d+),\s*(\d+),\s*(\d+),\s*(\d+)', rgba).groups()
        favorite_color = "rgb(" + r + ", " + g + ", " + b + ")"
        return favorite_color

    def __add_session_cookie_to_chrome(self):
        for cookie in self.s.cookies:
            cookie_dict = {'name': cookie.name, 'value': cookie.value}
            self.driver.add_cookie(cookie_dict)

    # ------------------------------------------------------------------------------------------------------------
    # ------ I leave those methods here in case server will not get internal errors often and we would use it-----
    # ------------------------------------------------------------------------------------------------------------
    def get_user_followers(self, user_id):
        followers_map = {}
        json_response = self.get_followers_as_json(followers_id_to_follower=followers_map, skip=0, user_id=user_id)

        while json_response['more']:
            json_response = self.get_followers_as_json(followers_id_to_follower=followers_map, skip=len(followers_map),
                                                       user_id=user_id)
        return followers_map

    def get_followers_as_json(self, followers_id_to_follower, skip, user_id):
        followers = self.s.get(url="http://35.188.78.78:8898/api/user/" + user_id + "/followers?skip=" + str(skip))
        json_response = json.loads(followers.text)
        for follower in json_response['followers']:
            followers_id_to_follower[follower['id']] = follower
        return json_response
