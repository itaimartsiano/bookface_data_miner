class UserData:
    user_id = ''
    name = ''
    age = ''
    favorite_color = ''
    followers = list()
    following = list()

    def __init__(self, user_id, name, age, favorite_color, followers, following):
        self.user_id = user_id
        self.name = name
        self.age = age
        self.favorite_color = favorite_color
        self.followers = followers
        self.following = following

    def get_row(self):
        return [self.user_id, self.name, self.age, self.favorite_color, self.followers, self.following]
