# queue with contains method
from queue import Queue


class UniqueQueue:
    objects_set = set()
    queue = Queue()

    def enqueue(self, element):
        if element not in self.objects_set:
            self.queue.put(element)
            self.objects_set.add(element)

    def deque(self):
        if self.queue.empty():
            return None
        element = self.queue.get()
        self.objects_set.remove(element)
        return element

    def is_empty(self):
        self.queue.empty()

    def contains(self, element):
        return self.objects_set.__contains__(element)
